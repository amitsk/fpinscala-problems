package fpinscala.datastructures

import scala.annotation.tailrec

sealed trait Tree[+A]

case class Leaf[A](value: A) extends Tree[A]

case class Branch[A](left: Tree[A], right: Tree[A]) extends Tree[A]

object Tree {

  def size[A](tree: Tree[A]): Int = tree match {
    case Branch(left, right) => 1 + size(left) + size(right)
    case Leaf(_) => 1
  }

  def maximum(tree: Tree[Int]): Int = tree match {
    case Branch(left, right) => maximum(left).max(maximum(right))
    case Leaf(value) => value
  }

  def depth(tree: Tree[Int]): Int = tree match {
    case Branch(left, right) => 1 + depth(left).max(depth(right))
    case Leaf(_) => 0
  }
  /*def fold(z: Int)(f: , tree: Tree[Int]): Int = tree match {
    case Branch(left, right) => maximum(left).max(maximum(right))
    case Leaf(value) => value
  }*/


  def main(args: Array[String]): Unit = {

    println("Size of tree is" + Tree.size(Branch(Leaf(1), Leaf(2))))
    println("Size of 1 tree is" + Tree.size(Branch(Branch(Leaf(1), Leaf(2)),
                                                        Branch(Leaf(3), Leaf(4)))))

    println("maximum of a tree is" + Tree.maximum(Branch(Branch(Leaf(1000), Leaf(2)),
                                                        Branch(Leaf(3), Leaf(400)))))

    println("Depth of a tree 2 is" + Tree.depth(Branch(Branch(Leaf(1000), Leaf(2)),
                                                          Branch(Leaf(3), Leaf(400)))))

    println("Depth of a tree 1 is" + Tree.depth(Branch(Leaf(1000), Leaf(2))))

    println("Depth of a tree 3 is" + Tree.depth(Branch(Branch(Leaf(1000), Leaf(2)),
                                                          Branch(Branch(Branch(Leaf(1000), Leaf(2)), Leaf(2)), Leaf(400)))))
  }
}