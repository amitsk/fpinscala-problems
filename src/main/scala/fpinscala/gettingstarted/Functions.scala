package fpinscala.gettingstarted

import scala.annotation.tailrec

/**
  * Created by akuma9 on 2/27/17.
  */
object Functions {
  def abs(n: Int): Int = if (n < 0) -n else n

  private def formatAbs( x:Int) = {
    val msg = "Abs val of %d is %d"
    msg.format(x, abs(x))
  }


  def main(args: Array[String]): Unit = {
    println(formatAbs( -42))
    println(factorial(0))
    println(factorial(1))
    println(factorial(2))
    println(factorial(3))
  }

  def factorial(n: Int): Int = {
    @tailrec
    def loop(n: Int, acc: Int): Int = {
      if (n <= 0) {
        acc
      } else {
        loop(n - 1, acc * n)
      }
    }
    loop(n, 1)
  }

  def fib(n: Int): Int = {

    def go ( n: Int, prev: Int, next: Int): Int = {
      if ( n <= 1) next
      else go( n -1, next, prev + next)
    }

    go(n, 0, 1)

  }

  def isSorted[A](as: Array[A], ordered: (A,A) => Boolean): Boolean = ???

  def findFirst[A](as:Array[A], p: A =>Boolean ): Int = {
    @tailrec
    def loop( n:Int): Int = {
      if (n > as.length) -1
      else if(p( as(n))) n
      else loop( n + 1 )
    }
    loop(0)
  }

  def partial1[A, B, C](a: A, f: (A, B) => C): B => C = b => f(a, b)

  def curry[A,B,C](f: (A, B) => C): A => (B => C) = {
    a => b => f(a, b)
  }

  def uncurry[A,B,C](f: A => B => C): (A, B) => C = {
    (a:A, b:B) => f(a)(b)
  }

  def compose[A,B,C](f: B => C, g: A => B): A => C = {
    a:A => f(g(a))
  }

}
