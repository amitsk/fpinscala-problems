package fpinscala.laziness

import org.scalatest._

/**
  * Created by amit on 5/28/17.
  */
class StreamSpec  extends FlatSpec with Matchers{

  it should " takeWhile correctly " in {
    import Stream._
    val stream = cons(1, cons(2, cons(3, Empty)))
    stream.takeWhile( _ %2 == 0).toList shouldBe( List())
  }

  it should " map correctly " in {
    import Stream._
    val stream = cons(1, cons(2, cons(3, Empty)))
    stream.map( _ * 100).headOption shouldBe( Some(100))
  }

  it should " filter correctly " in {
    import Stream._
    val stream = cons[Int](6, cons(2, cons(3, Empty)))
    stream.filter( _ % 3 == 0).take(2).toList shouldBe List(6, 3)
  }

  it should " append correctly " in {
    import Stream._
    val stream = Empty
    stream.append(cons(1, Empty)).toList shouldBe List(1)
  }

  it should " fibs2 correctly " in {
    import Stream._

    Stream.fibs2.take(1).toList shouldBe Some(0)
  }

}
