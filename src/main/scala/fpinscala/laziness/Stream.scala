package fpinscala.laziness

import Stream._
trait Stream[+A] {

  def foldRight[B](z: => B)(f: (A, => B) => B): B = // The arrow `=>` in front of the argument type `B` means that the function `f` takes its second argument by name and may choose not to evaluate it.
    this match {
      case Cons(h, t) =>
        f(h(), t().foldRight(z)(f)) // If `f` doesn't evaluate its second argument, the recursion never occurs.
      case _ => z
    }

  def foldRight2[B](z: => B)(f: (A, => B) => B): B = {
    this match {
      case Cons(h, t) => f(h(), t().foldRight2(z)(f))
      case Empty => z
    }
  }

  def exists2(p: A => Boolean): Boolean =
    foldRight2(false)((a, z) => p(a) || z)

  def exists(p: A => Boolean): Boolean =
    foldRight(false)((a, b) => p(a) || b) // Here `b` is the unevaluated recursive step that folds the tail of the stream. If `p(a)` returns `true`, `b` will never be evaluated and the computation terminates early.

  @annotation.tailrec
  final def find(f: A => Boolean): Option[A] = this match {
    case Empty => None
    case Cons(h, t) => if (f(h())) Some(h()) else t().find(f)
  }

  private def takeOrDrop(n: Int, condition: (Int) => Boolean): Stream[A] = {
    this match {
      case Empty => Empty
      case Cons(h, t) =>
        if (condition(n)) cons(h(), takeOrDrop(n - 1, condition))
        else takeOrDrop(n - 1, condition)
    }
  }
//  def take(n: Int): Stream[A] = {
//    takeOrDrop(n, _ > 0)
//  }

  def take(n: Int): Stream[A] = {
    foldRight[(Stream[A], Int)]((Empty, n))((a, tuple) => if (tuple._2 == 0) (Empty, 0) else (cons(a, tuple._1), n - 1))._1
  }

  def drop(n: Int): Stream[A] = takeOrDrop(n, _ < 0)

  def takeWhile(p: A => Boolean): Stream[A] =
    foldRight[Stream[A]](Empty)((a, z) => if (!p(a)) Empty else cons(a, z))

  def forAll(p: A => Boolean): Boolean = !exists(!p(_))

  def headOption: Option[A] = foldRight(None: Option[A])((h, _) => Some(h))

  // 5.7 map, filter, append, flatmap using foldRight. Part of the exercise is
  // writing your own function signatures.

  def map[B](f: A => B): Stream[B] =
    foldRight[Stream[B]](Empty)((a, z) => cons(f(a), z))

  def filter(p: A => Boolean): Stream[A] =
    foldRight[Stream[A]](Empty)((e, z) => if (p(e)) cons(e, z) else z)

  def append[B >: A](rest: => Stream[B]): Stream[B] =
    foldRight(rest)((h, t) => cons(h, t))

  def flatmap[B](p: A => Stream[B]): Stream[B] =
    foldRight[Stream[B]](Empty)((a, z) => p(a).append(z))

  def startsWith[B](s: Stream[B]): Boolean = ???

  def toList: List[A] = this match {
    case Empty => Nil
    case Cons(h, t) => h() :: t().toList
  }
}
case object Empty extends Stream[Nothing]
case class Cons[+A](h: () => A, t: () => Stream[A]) extends Stream[A]

object Stream {
  def cons[A](hd: => A, tl: => Stream[A]): Stream[A] = {
    lazy val head = hd
    lazy val tail = tl
    Cons(() => head, () => tail)
  }

  def empty[A]: Stream[A] = Empty

  def apply[A](as: A*): Stream[A] =
    if (as.isEmpty) empty
    else cons(as.head, apply(as.tail: _*))

  val ones: Stream[Int] = Stream.cons(1, ones)

  def constant[A](a: A): Stream[A] = Stream.cons(a, constant(a))

  def from(n: Int): Stream[Int] = {
    cons(n, from(n - 1))
  }

  def fibs: Stream[Int] = {
    def fib(a: Int, b: Int): Stream[Int] = cons[Int](a, fib(b, a + b))
    fib(0, 1)
  }

  def unfold[A, S](z: S)(f: S => Option[(A, S)]): Stream[A] =
    f(z).map { case (a: A, s: S) => cons(a, unfold(s)(f)) }.getOrElse(Empty)

  def fibs2: Stream[Int] = {
    val fib: ((Int, Int)) => Option[(Int, (Int, Int))] = {
      case (v1, v2) => Some(v1, (v2, v1 + v2))
    }
    unfold[Int, (Int, Int)](0, 1)(fib)
  }
}
