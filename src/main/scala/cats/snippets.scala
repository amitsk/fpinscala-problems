case class PreferredPrompt(val preference: String) 

object Greeter {
    def greet(name: String)(implicit prompt: PreferredPrompt) = {
      println("Welcome, " + name + ". The system is ready.")
      println(prompt.preference)
    }
}

object AmitsPrompts {
    implicit val myPrompt = PreferredPrompt("Me")
}