package fpinscala.errorhandling

import org.scalatest._



class OptionSpec extends FlatSpec with Matchers {

  import scala.{Option => _, Some => _, Either => _, _} // hide std library `Option`, `Some` and `Either`, since we are writing our own in this chapter
  import fpinscala.errorhandling.None
  import fpinscala.errorhandling.Some

  behavior of "Option"

  it should "map correctly" in {
    val o1 = Some(1000)
    val o2 = None
    o1.map(_ * 2) shouldBe Some(2000)
    o2 map( _ => 1000) shouldBe None
  }

  it should "calculate Variance correctly" in {
    Option.variance(Seq(1,2,3,4,5)) shouldBe Some(2)
    Option.variance(Seq()) shouldBe None
  }

  it should " Map map2 correctly " in {
    Option.map2[Int, Int, Int](Some(1), Some(2))((x: Int, y: Int) => x + y) shouldBe Some(3)
    Option.map2[Int, Int, Int](None, Some(2))((x: Int, y: Int) => x + y) shouldBe None
    Option.map2[Int, Int, Int](Some(1), None)((x: Int, y: Int) => x + y) shouldBe None
  }

  it should " take(N) correctly " in {
    import Stream._
    println(cons( 1, cons(2, cons(3, Empty))).take(1).take(1) + "")

  }

  it should " drop(N) correctly " in {
    import Stream._
    println(cons( 1, cons(2, cons(3, Empty))).drop(1).drop(1) + "")

  }
}
