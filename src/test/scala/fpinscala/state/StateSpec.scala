package fpinscala.state


import fpinscala.state.RNG.Simple
import org.scalatest.{FlatSpec, Matchers}


class StateSpec extends FlatSpec with Matchers {
  it should " handle State ints  correctly" in {
    RNG.ints(2)(Simple(120L))._1.size shouldBe 2
  }



}
