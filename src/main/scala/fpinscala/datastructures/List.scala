package fpinscala.datastructures

sealed trait List[+A] // `List` data type, parameterized on a type, `A`
case object Nil extends List[Nothing] // A `List` data constructor representing the empty list
/* Another data constructor, representing nonempty lists. Note that `tail` is another `List[A]`,
which may be `Nil` or another `Cons`.
 */
case class Cons[+A](head: A, tail: List[A]) extends List[A]

object List { // `List` companion object. Contains functions for creating and working with lists.

  def apply[A](as: A*): List[A] = // Variadic function syntax
    if (as.isEmpty) Nil
    else Cons(as.head, apply(as.tail: _*))


  def foldRight[A,B](as: List[A], z: B)(f: (A, B) => B): B = // Utility functions
    as match {
      case Nil => z
      case Cons(x, xs) => f(x, foldRight(xs, z)(f))
    }


  def tail[A](l: List[A]): List[A] = drop( l,1 )

  def setHead[A](l: List[A], h: A): List[A] = l match {
    case Nil => Nil
    case Cons(x, Nil) => Cons(h, Nil)
    case Cons(x, xs) => Cons(h, xs)
  }

  def drop[A](l: List[A], n: Int): List[A] = {
    def loop(l: List[A], counter: Int): List[A] = {
      if (counter >= n) l
      else loop(getTail(l), counter + 1)
    }

    def getTail[A](l: List[A]): List[A] = l match {
      case Nil => Nil
      case Cons(x, xs) => xs
    }
    loop(l, 0)
  }

  def dropWhile[A](l: List[A], f: A => Boolean): List[A] = {
    def loop(l: List[A], f: A => Boolean): List[A] = l match {
      case Nil => Nil
      case Cons(a, as) => if (!f(a)) l else loop(getTail(l), f)
    }

    def getTail[A](l: List[A]): List[A] = l match {
      case Nil => Nil
      case Cons(x, xs) => xs
    }
    loop(l, f)
  }

  def init[A](l: List[A]): List[A] = sys.error("todo")

  def length[A](l: List[A]): Int = foldLeft(l, 0)( ( x, _ ) => x + 1)

  def foldLeft[A,B](l: List[A], z: B)(f: (B, A) => B): B =  l match {
    case Nil => z
    case Cons(x, xs) => foldLeft(xs, f(z, x))(f)
  }

  def map[A, B](l: List[A])(f: A => B): List[B] = {
    def loop(l: List[A], f: A => B, acc: List[B]): List[B] = l match {
      case Nil => acc
      case Cons(x, xs) => loop(xs, f, Cons(f(x), acc))
    }
    loop(l, f, List())
  }

}

object Main {
  def main(args: Array[String]) {
//    println(List.tail(List(1, 2, 3, 4)))
    //println(List.length(List(1.5, 2, 3, 4)))
    println(List.map[Double,Double](List(1.5, 2, 3, 4))( (x:Double) => x * 2  ))
//    println(List.setHead(List(1, 2, 3, 4), 99 ))
//    println(List.setHead(Nil, 99 ))
//    println(List.setHead(List(1), 99 ))
//    println(List.drop(List(1, 2, 3, 4), 2 ))
//    println(List.dropWhile[Int](List(8, 2, 4, 5,7 ), x => x % 2 == 0  ))
//    println(List.foldLeft(List(1,2,3,4,5,6,7), 0) ((x,y ) => x + y ))
//    println(List.foldLeft(List(1.0,2.0,3.0), 0) ((x,_  ) => x + 1))
  }
}
