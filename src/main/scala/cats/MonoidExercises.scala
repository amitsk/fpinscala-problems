package cats

import cats.MonoidExercises.Monoid

object MonoidExercises {
    def add[T](items: List[T])(implicit monoid:Monoid[T]): T = {
        items.foldLeft( monoid.empty)( _ |+| _)
    }

     def add[A:Monoid](items: List[A]): A = {
        items.foldLeft( Monoid[A].empty)( _ |+| _)
    }

    /*
    trait Monoid[A] extends Semigroup[A] {
  def empty: A
}
object Monoid {
  def apply[A](implicit monoid: Monoid[A]) =
monoid }
*/

    case class Order(totalCost: Double, quantity: Double)

    implicit val orderMonoid = new Monoid[Order] {
        def empty = Order(0.0, 0)
        def combine(x: Order, y: Order): Order = Order( x.totalCost + y.totalCost, x.quantity + y.quantity)
    }

  trait Monoid[A] {
    def op(a1: A, a2: A): A
    def zero: A
  }

  val stringMonoid = new Monoid[String] {
    override def op(a1: String, a2: String) = a1 + a2
    override def zero = ""
  }

  def listMonoid[A] = new Monoid[List[A]] {
    override def op(a1: List[A], a2: List[A]) = a1 ++ a2
    override def zero = Nil
  }

  val intAddition= new Monoid[Int] {
    override def op(a1: Int, a2: Int) = a1 + a2
    override def zero = 0
  }

  val intMultiplication= new Monoid[Int] {
    override def op(a1: Int, a2: Int) = a1 * a2
    override def zero = 1
  }

  val booleanOr= new Monoid[Boolean] {
    override def op(a1: Boolean, a2: Boolean) = a1 || a2
    override def zero = true
  }

  val booleanAnd= new Monoid[Boolean] {
    override def op(a1: Boolean, a2: Boolean) = a1 && a2
    override def zero = false
  }

  def optionMonoid[A]: Monoid[Option[A]] = new Monoid[Option[A]] {
    override def op(a1: Option[A], a2: Option[A]) = a1.flatMap( _ => a2)
    override def zero = None
  }

  def endoMonoid[A]: Monoid[A => A] = new Monoid[ A => A] {
    override def op(a1: (A) => A, a2: (A) => A) = a1.andThen(a2)
    override def zero = a => a
  }
}
