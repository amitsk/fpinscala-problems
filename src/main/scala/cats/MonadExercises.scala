package cats

import cats.syntax.flatMap._
import cats.syntax.functor._

import scala.language.higherKinds

object MonadExercises {
  def sumSquare[M[_] : Monad](a: M[Int], b: M[Int]): M[Int] =
    for {
      x <- a
      y <- b
    } yield x * x + y * y

}
