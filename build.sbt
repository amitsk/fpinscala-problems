name := "fpinscala-problems"

version := "1.0"

scalaVersion := "2.12.1"

libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.1"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.1" % "test"
libraryDependencies += "org.typelevel" %% "cats-core" % "1.0.0-MF"
libraryDependencies += "org.typelevel" %% "cats-kernel" % "1.0.0-MF"
libraryDependencies += "org.typelevel" %% "cats-macros" % "1.0.0-MF"
    